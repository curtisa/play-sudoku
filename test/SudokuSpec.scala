import controllers.Application
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class SudokuSpec extends Specification {

  "Sudoku app" should {

    "generate a integer based grid" in {
      // We manually select the list of ints that will be our last row
      val lastRowOfInts = List(4, 7, 5, 3, 8, 1, 9, 2, 6)

      // Now we generate the sudoku grid
      val resultingGrid = Application.createRows(lastRowOfInts)

      System.out.println("Our completed Sudoku grid is : ")
      System.out.println( resultingGrid.mkString("\n") )

      "with the last row being List (4, 7, 5, 3, 8, 1, 9, 2, 6)" in {
        resultingGrid.last must beEqualTo( List(4, 7, 5, 3, 8, 1, 9, 2, 6) )
      }

      "with a total of 9 rows" in {
        resultingGrid.length must beEqualTo(9)
      }

      "with the sum of all numbers in the left hand column equal to 45" in {
        resultingGrid.map ( row => row.head).sum must beEqualTo(45)
      }

      "with the sum of ALL numbers in our grid totalling 405 (45 x 9)" in {
        resultingGrid.map(_.sum).sum must beEqualTo(405)
      }

      "with the left hand column of rows 1 to 8 NOT equal to 4" in {
        resultingGrid.take(8).map( row => row.head != 4).forall (_ == true) must beTrue
      }

      "with the last 3 items in rows 7 and 8 NOT having 9, 2 or 6" in {
        resultingGrid.drop(6).take(2).map ( row =>
          row.drop(6).forall( ! List(9,2,6).contains( _ ) )
        ).forall( _ == true) must beTrue
      }
    }

    "generate a character based grid" in {
      // We manually select the list of chars that will be our last row
      val lastRowOfChars = List("A", "B", "C", "D", "E", "F", "G", "H", "I")

      // Now we generate the sudoku grid
      val resultingGrid = Application.createRows(lastRowOfChars)

      System.out.println("Our completed Sudoku grid is : ")
      System.out.println( resultingGrid.mkString("\n") )

      "with the last row being List('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I')" in {
        resultingGrid.last must beEqualTo( List("A", "B", "C", "D", "E", "F", "G", "H", "I") )
      }

      "with a total of 9 rows" in {
        resultingGrid.length must beEqualTo(9)
      }

      "with the left hand column of rows 1 to 8 NOT equal to A" in {
        resultingGrid.take(8).map( row => row.head != "A").forall (_ == true) must beTrue
      }

      "with the last 3 items in rows 7 and 8 NOT having G, H or I" in {
        resultingGrid.drop(6).take(2).map ( row =>
          row.drop(6).forall( ! List("G","H","I").contains( _ ) )
        ).forall( _ == true) must beTrue
      }
    }

  }
}
