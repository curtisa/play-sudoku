# play-sudoku

This is a simple Scala based Play app which demonstrates a way to build up a Sudoku grid using tail recursive functions in Scala. 

It builds up the grid as a `List [ List [ T ] ]` by:

- being supplied a random list of T as the last row; then
- build up the grid 1 row at a time by adding a new row to our list of rows; with
- the T on each row being built up by successively adding a random T to the head element of the current row.
- If a row turns out to be unsolvable (because of a previously selected T) then the entire row is re-calculated.

## To test 

1. Install play from [https://www.playframework.com](https://www.playframework.com/)
2. Set PATH environment so `activator` works from any dir
3. Check out (and cd to) this repo
4. $ activator test


## A nice UI

To be done !