package controllers

import play.api.mvc._
import scala.util.Random

object Application extends Controller {

  // This defines our dimension (ie typically 9x9)
  val sizeOfGrid = 9
  
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  /**
   * @author  Alex Curtis
   * @return  Unit
   *
   * Starting def.
   * - When referring to rows and columns in code, assume starting from top left, so
   * - row \ col | 1 | 2 | 3 | .. | 9 |
   *       1     | x | x | x | .. | x |
   *       2     | x | x | x | .. | x |
   *       3     | x | x | x | .. | x |
   *       ..    | . | . | . | .. | . |
   *       9     | x | x | x | .. | x |
   *
   * - Since using a list of list of elems structure, we'll be adding to head of lists
   *   at all times. This means we build up our grid from col 9 of row 8 first
   *   (row 8 because the 9th row will be generated from a random list of 1 .. 9)
   *
   * - We then use two tail recursive functions to build up our grid
   */
  def generateGrid[T]( lastRow: List[T] ) = {
    // Call our tail recursive function to create all the rows in our grid
    // (passing our randomly generated row in as the last row)
    val resultingGrid = createRows(lastRow)

    System.out.println("Our completed Sudoku grid is : ")
    System.out.println( resultingGrid.mkString("\n") )
  }


  /**
   * Generate a list of list of ints
   *
   * @param  lastRow  the initial last row of our sudoku grid
   * @return          a list of list of elems representing our completed grid
   *
   */
  def createRows[T](lastRow : List[T]): List[List[T]] = {

    /**
     * We pass our current list of rows in here because we need to know them
     * in order to make sure that
     *   a) we don't duplicate numbers column-wise AND
     *   b) we don't duplicate within the 3x3 square
     *
     * @param listOfRows Our list of existing rows
     * @return
     */
    def generateRow(listOfRows: List[List[T]]) = {

      /**
       * This is where we calculate what can go into an individual cell
       *
       * @param currentRowElems The list of elems already in our current row
       * @param listOfRows      The list of rows already in our current grid
       * @return                The new list of elems in our current row
       */
      def addNewElem(currentRowElems: List[T], listOfRows: List[List[T]] ): List[T] = {

        // We'll use a random list of elems from which we'll pick our new elem
//        val randomListOfTs: List[T] = Random.shuffle(lastRow.toList)
        val randomListOfTs: List[T] = Random.shuffle(lastRow diff currentRowElems)

        // The list of available elements that we can put in the current cell is reduced by
        // 1 - Whatever elements are already in the current row ('currentRowElems')
        // 2 - Whatever elements are already in the current column ('currentColTs')
        // 3 - Whatever elements are already in the current 3x3 grid ('currentGridTs')

        // Our currentColTs is just the n'th index item from all our previous rows
        val currentColTs: List[T] = listOfRows.map (_(sizeOfGrid - currentRowElems.length - 1) )

        // When working out the elements in our current 3x3 grid, we only need to consider
        // those elements in our 3x3 grid that are on previous rows (because
        // 'currentRowTs' will take care of our neighbours on the current row)
        // - If we are on rows 3, 6 or 9 then we are on the bottom row of our 3x3 grid (so nothing to return)
        // - If we are on any other row then for each row use a combination of
        //   'drops' and 'takes' to get the 3 elements that are in our current 3x3 grid.
        val currentGridTs: List[T] = listOfRows.length % 3 match {
          case 0          => List()                                       // We're on rows 3, 6 or 9 so there are no rows under us
          case x if x > 0 =>                                              // We have either 1 or 2 rows under us
            listOfRows.take(x).map ( row =>
              row.drop( (sizeOfGrid - 1 - currentRowElems.length ) / 3 * 3).take(3).toList
            ).flatten
        }

        // Our list of possible elems that we can put in the current cell is our
        // list of random elems less the 'current(Row/Col/Grid) elems'
        val possibleTs: List[T] =
            randomListOfTs diff                                           // Our original list of random elems less
            currentRowElems diff                                             // elems in our current row less
            currentColTs diff                                             // elems in current column less
            currentGridTs                                                 // elems in our 3x3 grid

        if (possibleTs.isEmpty) {
          System.out.println(" - List of possible elements is zero, trying row " + listOfRows.size + " again")
          System.out.println("   - Current row is : " + currentRowElems.mkString(","))
        }

        // If our list of possibleTs is empty then this means an element was chosen earlier in this current row
        // that would cause the current row to be unsolvable (because 1 of the 3 criteria no longer holds).
        // We could 'backup' one cell at a time to try again, or simply force the entire current row
        // to be re-calculated. Recalculating the entire row can be done by simply returning an empty list as
        // our list for the current row.
        possibleTs match {
            case Nil        => List()                                     // Empty list causes current row to be re-calculated
            case _          => possibleTs.head :: currentRowElems         // Simply take the first int in our list of possible ints
        }
      }

      /**
       * Tail recursively build up our columns of elems within our current row
       *
       * @param  currentRowElems  The list of elems in our current row (initially will be an empty list)
       * @param  listOfRows       The list of rows built up so far (note we need this so we can work out our 3x3 neighbours)
       * @return                  The list of elems in our current row (to which we'll recursively add a new elem)
       */
      def recurseOnColumns ( currentRowElems: List[T], listOfRows: List[List[T]] ) : List[T] = currentRowElems.length match {
          case x if x == sizeOfGrid   => currentRowElems
          case _                      => recurseOnColumns ( addNewElem(currentRowElems, listOfRows), listOfRows )
      }

      // Call our tail recursive def to build up a single row
      recurseOnColumns ( List(), listOfRows )
    }
    
    /**
     * Tail recursively build up our rows, starting from the lastRow (row 9) and working up to our first row (row 1)
     *
     * @param  listOfRows  Our initial list of list of elems (initially just our lastRow)
     * @return             Our listOfRows with a new list of elems added at the head
     */
    def recurseOnRows (listOfRows: List[List[T]]): List[List[T]] = listOfRows.length match {
        case x if x == sizeOfGrid => listOfRows                                 // If number of rows equals size of grid we're finished
        case _ => recurseOnRows ( generateRow( listOfRows ) :: listOfRows)        // Otherwise need to recursively build up our rows (from the bottom up remember)
    }

    // Call our tail recursive def to build up a list of rows
    recurseOnRows ( List(lastRow) )
  }
 
}